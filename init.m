% global N

N = 2;
w = zeros(N, 1);
w(1) = 0; %the initial radial velocity of bubble
w(2) = 0; %the initial radial velocity of particle ( w_{2} = const = 0 )
% u = zeros(3*N, 1);
u = [1; -1];
% u(1,:) = [0 0 0];
% u(2,:) = [0 0 0];

a = zeros(2, 1);
a(1) = 1e-5; %the initial radius of bubble
a(2) = 1e-5; %the initial radius of particle
m = zeros(2, 1);
m(1) = 0;
m(2) = ( 4/3*pi*a(2)^3 ) * rho_p; % 2000 is density of particle
Z = zeros(2, 1);
Z = [0; d];
% r(1,:) = [0 0 0];
% r(2,:) = [d 0 0]; % the particle is located in (d, 0, 0) point