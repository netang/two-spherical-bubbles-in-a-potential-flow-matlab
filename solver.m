function dy = solver( t, y )
% solver for ode45 function
global N m p
global rho_l P0 Ap omega gamma sigma Pg0 R01 T

%   y = [ [psi1 psi2 ... ] [p1 p2 ... ] [a1 a2 ... ] [Z1 Z2 ... ] ];
psi = y(1:2);
pj   = y(3:4);
a   = y(5:6);
Z   = y(7:8);
d = Z(2) - Z(1);


p_inf = P0;% - Ap*sin(omega*t);
p_S = zeros(N, 1);
for i = 1:N
    p_S(i) = Pg0*(R01/a(i))^(3*gamma)-2*sigma/a(i);
end

P1 = zeros(p, 1);
P2 = zeros(p, 1);
for i = 0:p-1
    P1(i+1) = -psi(2) / (4*pi*rho_l*a(2)) * ...
          T(i+1, 1)/(-d)^(i+1)...
         -1/2*a(2)^3*pj(2)/(m(2)+2/3*pi*rho_l*a(2)^3)*...
          T(i+1, 2)/(-d)^(i+1+1);
    P2(i+1) = -psi(1)/ (4*pi*rho_l*a(1)) * ...
          T(i+1, 1)/( d)^(i+1)...
         -1/2*a(1)^3*pj(1)/(m(1)+2/3*pi*rho_l*a(1)^3)*...
          T(i+1, 2)/( d)^(i+1+1);
end
P1
P2

L12 = zeros(p, p);
L21 = zeros(p, p);
for n = 0:p-1
    for j = 0:p-1
        L12(n+1, j+1) = -T(n+1, 1)/((-d)^(n+0+1))*a(2)*s_kron(j, 0) - ...
            (pi*rho_l*a(2)^6)/(m(2) + 2/3*pi*rho_l*a(2)^3)*...
            T(n+1, 2)/((-d)^(n+1+1))*s_kron(j, 1) + ...
            j/(j+1)*  T(n+1, j+1)/((-d)^(n+j+1))  *a(2)^(2*j+1);
        L21(n+1, j+1) = -T(n+1, 1)/(( d)^(n+0+1))*a(1)*s_kron(j, 0) - ...
            (pi*rho_l*a(1)^6)/(m(1) + 2/3*pi*rho_l*a(1)^3)*...
            T(n+1, 2)/(( d)^(n+1+1))*s_kron(j, 1) + ...
            j/(j+1)*  T(n+1, j+1)/(( d)^(n+j+1))  *a(1)^(2*j+1);
    end
end
L12
L21

B = [eye(p) L12; L21 eye(p)]\[P1; P2];
B

w = zeros(2,1);
u = zeros(2,1);
for j = 1:2
    w(j) = 1/a(j)*(psi(j)/(4*pi*rho_l*a(j)^2) + B(j*p-p+1));
    u(j) = (pj(j) + 2*pi*rho_l*a(j)^3*B(j*p-p+2))/(m(j)+2/3*pi*rho_l*a(j)^3);
end

dKda = zeros(2,1);
dKdZ = zeros(2,1);

sum1 = zeros(2,1);
sum2 = zeros(2,1);
for j = 1:2 %
    for n = 1:p-1
        sum1(j) = n*(2*n+1)/(n+1)*a(j)^(2*n-2)*B(j*p-p+n)^2;
    end
end
for j = 1:2 %
    for n = 1:p-2
        sum2(j) = n*a(j)^(2*n-1)*B(j*p-p+n)*B(j*p-p+n+1);
    end
end


for j = 1:2
    dKda(j) = pi*rho_l*a(j)^2*( (6*w(j)-8/a(j)*B(j*p-p+1))*w(j) +...
        (u(j)-6*B(j*p-p+2))*u(j) + 2*sum1(j) );
end
for j = 1:2
    dKdZ(j) = pi*rho_l*a(j)^2*( -4*B(j*p-p+2)*w(j) - 3*a(j)*B(j*p-p+3)*u(j) + 4*sum2(j) );
end

dy = zeros(8,1);

for j = 1:2
    dy(j) = dKda(j) + 4*pi*a(j)^2*(p_S(j) - p_inf);
    dy(2+j) = dKdZ(j);
    dy(4+j) = w(j);
    dy(6+j) = u(j);  
end;



end

