function res = get_T( n, m, d )

res = (-1)^m*factorial(n+m)/(factorial(n)*factorial(m)*d^(n+m+1));

end

