
global N m p;
global k R01 R02 ap d T rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;
N = 2;
c = 1;
k = 10; %��� ������ k, ��� ������ ��������� ����� ��������� � ��������
R01 = 1e-5; % ������ ���������
R02 = c*(1e-5); % ������ �������
ap = R02;
d = R01+R02+k*R01; %��������� �� ������ �������� �� ������ �������
rho_l = 1000; % ��������� ��������
rho_p = 0;%2000; % ��������� �������, rho1 �� ����������, �.�. ������������ ��.
V02 = (4/3)*pi*R02^3; %�����  ������� (� ������ ������ �� �� ��������)
P0 = 1e+5; %����������� �������� (�������� � ��������)
gamma = 1.4; %����������� ���������
sigma = 0.073;
Pg0 = P0+2*sigma/R01;
Ap = P0/2;
nu = 200e+3;
omega = 2*pi*nu;
NT = 5;
tmax = NT/nu;
Nt=5001;
Toutput=linspace(0, tmax, Nt);
time = Toutput*nu;

init;
p = 3;

U1 = zeros(p, 1); % от 0 до p-1
U2 = zeros(p, 1);

% Вычислить T_{nm}, n и m до скольки? от 0 до p-1
T = zeros(p, p);
% for i = 0:p-1
%     for j = 0:p-1
%         T(i+1, j+1) = ( (-1)^j * factorial(i + j) ) /...
%                       ( factorial(i)*factorial(j) );
%     end
% end

% Эффективное построение матрицы T_{nm}
newT = zeros(p, p);
newT(1, 1) = ( (-1)^0 * factorial(0 + 0) ) /...
                      ( factorial(0)*factorial(0) );
for j = 1:p-1
        newT(1, j+1) = newT(1, j)*(-1)*(j) / j;
end
for i = 1:p-1
    newT(i+1, 1) = newT(i, 1)*(i)/i;
    for j = 1:p-1
        newT(i+1, j+1) = newT(i+1, j)*(-1)*(i+j) / j;
    end
end
T = newT;

for i = 0:p-1
    U1(i+1) = -a(2)^2*w(2)*T(i+1, 1)/(-d)^(i+1)...
              -1/2*a(2)^3*u(2)*T(i+1, 2)/(-d)^(i+1+1);
    U2(i+1) = -a(1)^2*w(1)*T(i+1, 1)/( d)^(i+1)...
              -1/2*a(1)^3*u(1)*T(i+1, 2)/( d)^(i+1+1);
end

tic
for n = 0:p-1
    for j = 0:p-1
%         M12(n+1, j+1) = j/(j+1)*(   ((-1)^j * factorial(n+j))/(factorial(n)*factorial(j)*(-d)^(n+j+1) )  )*a(2)^(2*j+1);
%         M21(n+1, j+1) = j/(j+1)*(   ((-1)^j * factorial(n+j))/(factorial(n)*factorial(j)*   d^(n+j+1) )  )*a(1)^(2*j+1);
        M12(n+1, j+1) = j/(j+1)*( T(n+1, j+1)/(-d)^(n+j+1) )*a(2)^(2*j+1);
        M21(n+1, j+1) = j/(j+1)*( T(n+1, j+1)/(d)^(n+j+1)  )*a(1)^(2*j+1);
    end
end
toc
M12
M21

B = [eye(p) M12; M21 eye(p)]\[U1; U2];
B


psi = zeros(2,1);
pj = zeros(2,1);
for j = 1:2
    
    psi(j) = 4*pi*rho_l*a(j)^2*...
        ( a(j)*w(j) - B(j*p-p+1) );
    pj(j)   = m(j)*u(j) + 2/3*pi*rho_l*...
        a(j)^3*(u(j) - 3*B(j*p-p+2));
end

%==========================


options = odeset('RelTol',1e-4);
[T,Y] = ode45(@solver,Toutput,[psi; pj; a; Z], options);

figure;
plot(time,  Y(:, 5),   time,  Y(:, 6), 'LineWidth',2 );
